import translator from '../translator';

function getCommerce(publicId) {
  const { comunicator, lang } = this;

  return new Promise((resolve, reject) => {
    if (!publicId) {
      reject(new Error(translator('Public Id is needed', lang)));
    } else {
      comunicator.get(`/commerce/${publicId}`)
        .then(res => resolve(res.data))
        .catch((error) => {
          if (error.response) {
            reject(new Error(translator(error.response.data, lang)));
          } else if (error.request) {
            reject(new Error(translator('We couldn\'t complete the request', lang)));
          } else {
            reject(new Error(translator('Unknow error', lang)));
          }
        });
    }
  });
}

export default getCommerce;
