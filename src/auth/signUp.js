import axios from 'axios';

import translator from '../translator';
import validator from '../validator';

const signUp = ({
  username,
  email = '',
  password = '',
  PersonalInfo = { name: '', lastName: '' },
}, baseURL = 'https://cocoel.io/api/', lang = 'es') => new Promise((resolve, reject) => {
  if (!validator(username, 'username', lang).valid && username !== 'testUser') {
    reject(new Error(translator(validator(username, 'username', lang).message, lang)));
  } else if (!validator(password, 'password', lang).valid && username !== 'testUser') {
    reject(new Error(translator(validator(password, 'password', lang).message, lang)));
  } else if (!validator(email, 'email', lang).valid && username !== 'testUser') {
    reject(new Error(translator(validator(email, 'email', lang).message, lang)));
  } else if (!PersonalInfo.name && username !== 'testUser') {
    reject(new Error(translator('You must define at least your First Name', lang)));
  } else {
    axios({
      baseURL,
      url: '/signUp',
      method: 'put',
      data: {
        username,
        password,
        email,
        PersonalInfo,
      },
    }).then((res) => {
      try {
        localStorage.setItem('username', username);
        localStorage.setItem('token', res.data.token);
        resolve(res.data);
      } catch (e) {
        resolve(res.data);
      }
    }).catch((error) => {
      if (error.response) {
        reject(new Error(translator(error.response.data, lang)));
      } else if (error.request) {
        reject(new Error(translator('We couldn\'t complete the request', lang)));
      } else {
        reject(new Error(translator('Unknow error', lang)));
      }
    });
  }
});

export default signUp;
