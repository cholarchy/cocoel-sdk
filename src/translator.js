export default (word, lang) => {
  if (lang === 'es') {
    switch (word) {
      case 'No Username or Password provided':
        return 'No se ha indicado un Nombre de Usuario o Contraseña';
      case 'We couldn\'t complete the request':
        return 'No hemos podido completar la petición';
      case 'Unknow error':
        return 'Error desconocido';
      case 'The Username cannot be empty':
        return 'El Nombre de Usuario no puede estar vacio';
      case 'The Username must have at least 6 characters':
        return 'El Nombre de Usuario debe tener mínimo 6 caracteres';
      case 'The Username must not have more than 18 characters':
        return 'El Nombre de Usuario no debe tener más de 18 caracteres';
      case 'The Username must be alphanumeric':
        return 'El Nombre de Usuario debe ser alfanumérico';
      case 'Password cannot be empty':
        return 'La Contraseña no puede estar vacia';
      case 'Password must have at least 6 characters':
        return 'La Contraseña debe tener mínimo 6 caracteres';
      case 'Password must not have more than 24 characters':
        return 'La Contraseña no debe tener más de 24 caracteres';
      case 'Email cannot be empty':
        return 'El Correo Electrónico no puede estar vacío';
      case 'Email has not valid format':
        return 'El Correo Electrónico no tiene un formato válido';
      case 'You must define at least your First Name':
        return 'Se debe proporcionar por lo menos el Nombre';
      case 'Username or Email alredy registered':
        return 'Nombre de Usuario o Correo Electrónico ya registrados';
      default:
        return word;
    }
  }

  return word;
};
