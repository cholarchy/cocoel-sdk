import translator from '../translator';

function startDeleteCommerce(publicId) {
  const { comunicator, lang } = this;

  return new Promise((resolve, reject) => {
    if (!publicId) {
      reject(translator('Public Id is needed', lang));
    } else {
      comunicator.delete(`/commerce/${publicId}`)
        .then(() => resolve())
        .catch((error) => {
          if (error.response) {
            reject(new Error(translator(error.response.data, lang)));
          } else if (error.request) {
            reject(new Error(translator('We couldn\'t complete the request', lang)));
          } else {
            reject(new Error(translator('Unknow error', lang)));
          }
        });
    }
  });
}

export default startDeleteCommerce;
