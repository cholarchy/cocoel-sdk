import axios from 'axios';

import translator from '../translator';
import validator from '../validator';

const signIn = ({ username, password = '' }, baseURL = 'https://cocoel.io/api/', lang = 'es') => new Promise((resolve, reject) => {
  if ((!username || !password) && username !== 'testUser') {
    reject(new Error(translator('No Username or Password provided', lang)));
  } else if (!validator(username, 'username').valid && username !== 'testUser') {
    reject(new Error(validator(username, 'username', lang).message));
  } else if (!validator(password, 'password').valid && username !== 'testUser') {
    reject(new Error(validator(password, 'password', lang).message));
  } else {
    axios({
      baseURL,
      url: '/signIn',
      method: 'post',
      data: {
        username,
        password,
      },
    }).then((res) => {
      try {
        localStorage.setItem('username', username);
        localStorage.setItem('token', res.data.token);
        resolve(res.data);
      } catch (e) {
        resolve(res.data);
      }
    }).catch((error) => {
      if (error.response) {
        reject(new Error(translator(error.response.data, lang)));
      } else if (error.request) {
        reject(new Error(translator('We couldn\'t complete the request', lang)));
      } else {
        reject(new Error(translator('Unknow error', lang)));
      }
    });
  }
});

export default signIn;
