const CocoelSDK = require('./index');

describe('auth', () => {
  describe('signIn', () => {
    describe('Username', () => {
      it('Don\'t accept empty username', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signIn({ username: '', password: 'SuperSecret' }))
          .rejects.toEqual(new Error('No se ha indicado un Nombre de Usuario o Contraseña'));
      });

      it('Don\'t accept username with less than 6 characters', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signIn({ username: 'foo', password: 'SuperSecret' }))
          .rejects.toEqual(new Error('El Nombre de Usuario debe tener mínimo 6 caracteres'));
      });

      it('Don\'t accept username with more than 18 characters', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signIn({ username: 'foosernamesuperveryextralarge', password: 'SuperSecret' }))
          .rejects.toEqual(new Error('El Nombre de Usuario no debe tener más de 18 caracteres'));
      });

      it('Only accept username with alphanumeric charters', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signIn({ username: 'foosername$', password: 'SuperSecret' }))
          .rejects.toEqual(new Error('El Nombre de Usuario debe ser alfanumérico'));
      });
    });

    describe('Password', () => {
      it('Don\'t accept empty password', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signIn({ username: 'foosername', password: '' }))
          .rejects.toEqual(new Error('No se ha indicado un Nombre de Usuario o Contraseña'));
      });

      it('Don\'t accept password with less than 6 characters', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signIn({ username: 'foosername', password: '1234' }))
          .rejects.toEqual(new Error('La Contraseña debe tener mínimo 6 caracteres'));
      });

      it('Don\'t accept password with more than 24 characters', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signIn({ username: 'foosername', password: 'superultratopsecretpasswordandunhackeable' }))
          .rejects.toEqual(new Error('La Contraseña no debe tener más de 24 caracteres'));
      });
    });

    describe('Connection', () => {
      it('Must only connect with right server', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signIn({ username: 'foosername', password: 'SuperSecret' }, 'https://another.site/api/'))
          .rejects.toEqual(new Error('No hemos podido completar la petición'));
      });

      it('Must return a JSON Web Token', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signIn({ username: 'foosername', password: 'SuperSecret' }, 'http://localhost/api/'))
          .resolves.toHaveProperty('token');
      });

      it('Must return token if the username is "testUser"', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signIn({ username: 'testUser' }, 'http://localhost/api/'))
          .resolves.toHaveProperty('token');
      });
    });
  });

  describe('signUp', () => {
    describe('Username', () => {
      it('Don\'t accept empty username', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signUp({
          username: '',
          password: 'SuperSecret',
          email: 'foo@bar.com',
          PersonalInfo: {
            name: 'Foo',
            lastName: 'Bar',
          },
        })).rejects.toEqual(new Error('El Nombre de Usuario no puede estar vacio'));
      });

      it('Don\'t accept username with less than 6 characters', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signUp({
          username: 'foo',
          password: 'SuperSecret',
          email: 'foo@bar.com',
          PersonalInfo: {
            name: 'Foo',
            lastName: 'Bar',
          },
        })).rejects.toEqual(new Error('El Nombre de Usuario debe tener mínimo 6 caracteres'));
      });

      it('Don\'t accept username with more than 18 characters', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signUp({
          username: 'foosernamesuperveryextralarge',
          password: 'SuperSecret',
          email: 'foo@bar.com',
          PersonalInfo: {
            name: 'Foo',
            lastName: 'Bar',
          },
        })).rejects.toEqual(new Error('El Nombre de Usuario no debe tener más de 18 caracteres'));
      });

      it('Only accept username with alphanumeric charters', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signUp({
          username: 'foosername$',
          password: 'SuperSecret',
          email: 'foo@bar.com',
          PersonalInfo: {
            name: 'Foo',
            lastName: 'Bar',
          },
        }))
          .rejects.toEqual(new Error('El Nombre de Usuario debe ser alfanumérico'));
      });
    });

    describe('Password', () => {
      it('Don\'t accept empty password', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signUp({
          username: 'foosername',
          password: '',
          email: 'foo@bar.com',
          PersonalInfo: {
            name: 'Foo',
            lastName: 'Bar',
          },
        })).rejects.toEqual(new Error('La Contraseña no puede estar vacia'));
      });

      it('Don\'t accept password with less than 6 characters', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signUp({
          username: 'foosername',
          password: '1234',
          email: 'foo@bar.com',
          PersonalInfo: {
            name: 'Foo',
            lastName: 'Bar',
          },
        })).rejects.toEqual(new Error('La Contraseña debe tener mínimo 6 caracteres'));
      });

      it('Don\'t accept password with more than 24 characters', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signUp({
          username: 'foosername',
          password: 'superultratopsecretpasswordandunhackeable',
          email: 'foo@bar.com',
          PersonalInfo: {
            name: 'Foo',
            lastName: 'Bar',
          },
        })).rejects.toEqual(new Error('La Contraseña no debe tener más de 24 caracteres'));
      });
    });

    describe('Email', () => {
      it('Don\'t accept empty email', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signUp({
          username: 'foosername',
          password: 'SuperSecret',
          email: '',
          PersonalInfo: {
            name: 'Foo',
            lastName: 'Bar',
          },
        })).rejects.toEqual(new Error('El Correo Electrónico no puede estar vacío'));
      });

      it('Don\'t accept invalid Email format', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signUp({
          username: 'foosername',
          password: 'SuperSecret',
          email: 'foo@bar',
          PersonalInfo: {
            name: 'Foo',
            lastName: 'Bar',
          },
        })).rejects.toEqual(new Error('El Correo Electrónico no tiene un formato válido'));
      });
    });

    describe('PersonalInfo', () => {
      it('Don\'t accept empty name', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signUp({
          username: 'foosername',
          password: 'SuperSecret',
          email: 'foo@bar.com',
          PersonalInfo: {
            name: '',
            lastName: '',
          },
        })).rejects.toEqual(new Error('Se debe proporcionar por lo menos el Nombre'));
      });
    });

    describe('Connection', () => {
      it('Must only connect with right server', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signUp({
          username: 'foosername',
          password: 'SuperSecret',
          email: 'foo@bar.com',
          PersonalInfo: {
            name: 'Foo',
            lastName: 'Bar',
          },
        }, 'https://another.site/api/'))
          .rejects.toEqual(new Error('No hemos podido completar la petición'));
      });

      it('Only accept unique username or email', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signUp({
          username: 'foosername',
          password: 'SuperSecret',
          email: 'foo@bar.com',
          PersonalInfo: {
            name: 'Foo',
            lastName: 'Bar',
          },
        }, 'http://localhost/api/'))
          .rejects.toEqual(new Error('Nombre de Usuario o Correo Electrónico ya registrados'));
      });

      it('Must return token if the username is "testUser"', () => {
        expect.assertions(1);

        return expect(CocoelSDK.signUp({ username: 'testUser' }, 'http://localhost/api/'))
          .resolves.toHaveProperty('token');
      });
    });
  });
});
