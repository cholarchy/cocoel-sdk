import axios from 'axios';
import io from 'socket.io-client';

import translator from './translator';
import { signIn, signUp, signOut } from './auth';
import { getUser, updateUser, suscribeUserChanges, removeUser } from './user';
import {
  getCommerces,
  suscribeCommercesChanges,
  addCommerce,
  getCommerce,
  suscribeCommerceChanges,
  updateCommerce,
  startDeleteCommerce,
  stopDeleteCommerce,
} from './commerce';

class SDK {
  constructor(token = localStorage.getItem('token'), baseURL = 'http://cocoel.io/api/', lang = 'es') {
    if (!token) throw new Error('No token was found, please provide one, signIn or signUp');

    this.token = token;
    this.username = localStorage.getItem('username');
    this.baseURL = baseURL;
    this.lang = lang;
    this.testJWTPassword = 'test';

    this.io = io;
    this.socket = this.io(`${this.constructor.getBaseURL(this.baseURL)}/${this.username}`, {
      extraHeaders: { Authorization: `Bearer ${this.token}` },
      origins: this.constructor.getBaseURL(this.baseURL),
    });

    this.comunicator = axios.create({
      baseURL,
      headers: { Authorization: `Bearer ${token}` },
    });
  }

  static getBaseURL(string) {
    const splited = string.split('/');

    if (splited[0] === 'http:' || splited[0] === 'https:') {
      return `${splited[0]}//${splited[2]}`;
    }

    return `${splited[0]}`;
  }

  static signIn(credentials, baseURL = 'http://cocoel.io/api/', lang = 'es') {
    return signIn(credentials, baseURL, lang);
  }

  static signUp(userData, baseURL = 'http://cocoel.io/api/', lang = 'es') {
    return signUp(userData, baseURL, lang);
  }

  signOut() {
    return signOut.bind(this)();
  }

  getUser() {
    return getUser.bind(this)();
  }

  updateUser(newUserData) {
    return updateUser.bind(this)(newUserData);
  }

  suscribeUserChanges(cb) {
    return suscribeUserChanges.bind(this)(cb);
  }

  unsuscribeUserChanges() {
    this.socket.removeListener('UPDATED_USER', this.getNewUser);
  }

  removeUser() {
    return removeUser.bind(this)();
  }

  getCommerces() {
    return getCommerces.bind(this)();
  }

  suscribeCommercesChanges(cb) {
    return suscribeCommercesChanges.bind(this)(cb);
  }

  unsuscribeCommercesChanges() {
    this.socket.removeListener('UPDATED_COMMERCES', this.getNewCommerces);
  }

  addCommerce(commerce) {
    return addCommerce.bind(this)(commerce);
  }

  getCommerce(publicId) {
    return getCommerce.bind(this)(publicId);
  }

  suscribeCommerceChanges(publicId, cb) {
    return suscribeCommerceChanges.bind(this)(publicId, cb);
  }

  unsuscribeCommerceChanges(publicId) {
    if (!publicId) throw new Error(translator('Public Id is needed', this.lang));

    this[`commerceSocket${publicId}`]
      .removeListener('UPDATED_COMMERCE', this[`getNewCommerce${publicId}`]);
  }

  updateCommerce(publicId, newCommerceData) {
    return updateCommerce.bind(this)(publicId, newCommerceData);
  }

  startDeleteCommerce(publicId) {
    return startDeleteCommerce.bind(this)(publicId);
  }

  stopDeleteCommerce(publicId) {
    return stopDeleteCommerce.bind(this)(publicId);
  }
}

module.exports = SDK;
