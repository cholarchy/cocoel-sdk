function signOut() {
  return new Promise((resolve) => {
    this.token = undefined;
    this.username = undefined;
    this.baseURL = undefined;
    this.lang = undefined;
    this.testJWTPassword = undefined;
    this.comunicator = undefined;

    localStorage.removeItem('token');
    localStorage.removeItem('username');

    this.socket.close();

    resolve();
  });
}

export default signOut;
