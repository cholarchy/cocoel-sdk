import iGetUser from './getUser';
import iUpdateUser from './updateUser';
import iSuscribeUserChanges from './suscribeUserChanges';
import iRemoveUser from './removeUser';

export const getUser = iGetUser;

export const updateUser = iUpdateUser;

export const suscribeUserChanges = iSuscribeUserChanges;

export const removeUser = iRemoveUser;
