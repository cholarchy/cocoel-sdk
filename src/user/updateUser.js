import translator from '../translator';

function updateUser(newUserData) {
  const { comunicator, lang } = this;

  return new Promise((resolve, reject) => {
    comunicator.put('/user', newUserData)
      .then(() => resolve())
      .catch((error) => {
        if (error.response) {
          reject(new Error(translator(error.response.data, lang)));
        } else if (error.request) {
          reject(new Error(translator('We couldn\'t complete the request', lang)));
        } else {
          reject(new Error(translator('Unknow error', lang)));
        }
      });
  });
}

export default updateUser;
