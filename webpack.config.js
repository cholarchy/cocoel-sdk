const webpack = require('webpack');
const path = require('path');
const MinifyPlugin = require("babel-minify-webpack-plugin");

module.exports = {
  entry: ['babel-polyfill', './src/sdk'],
  output: {
    path: path.resolve(__dirname, './'),
    filename: 'index.js',
    library: 'cocoel-sdk',
    libraryTarget: 'umd',
    umdNamedDefine: true,
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        loader: 'babel-loader',
        options: {
          presets: ['env'],
        },
      },
    ],
  },
  target: 'web',
  plugins: [
    new MinifyPlugin({}, { comments: false }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
  ],
};