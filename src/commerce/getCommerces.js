import translator from '../translator';

function getCommerces() {
  const { comunicator, lang } = this;

  return new Promise((resolve, reject) => {
    comunicator.get('/commerces')
      .then(res => resolve(res.data))
      .catch((error) => {
        if (error.response) {
          reject(new Error(translator(error.response.data, lang)));
        } else if (error.request) {
          reject(new Error(translator('We couldn\'t complete the request', lang)));
        } else {
          reject(new Error(translator('Unknow error', lang)));
        }
      });
  });
}

export default getCommerces;
