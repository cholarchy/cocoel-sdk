import translator from '../translator';

function suscribeCommerceChanges(publicId, cb) {
  if (!publicId) throw new Error(translator('Public Id is needed', this.lang));

  if (cb) {
    this[`getNewCommerce${publicId}`] = () => {
      this.getCommerce(publicId)
        .then(newCommerceData => cb(undefined, newCommerceData, this.getCommerce))
        .catch(error => cb(error, undefined, this.getCommerce));
    };
  }

  this.getCommerce(publicId).then((newCommerceData) => {
    if (cb) cb(undefined, newCommerceData, this.getCommerce);

    this[`commerceSocket${publicId}`] = this.io(`${this.constructor.getBaseURL(this.baseURL)}/${publicId}`, {
      extraHeaders: { Authorization: `Bearer ${this.token}` },
      origins: this.constructor.getBaseURL(this.baseURL),
    });

    this[`commerceSocket${publicId}`].on('UPDATED_COMMERCE', this[`getNewCommerce${publicId}`]);
  }).catch(error => cb(error, undefined, this.getCommerce));
}

export default suscribeCommerceChanges;
