import translator from '../translator';

function removeUser() {
  const { comunicator, lang } = this;

  return new Promise((resolve, reject) => {
    comunicator.delete('/user')
      .then(() => {
        localStorage.removeItem('token');

        resolve();
      })
      .catch((error) => {
        if (error.response) {
          reject(new Error(translator(error.response.data, lang)));
        } else if (error.request) {
          reject(new Error(translator('We couldn\'t complete the request', lang)));
        } else {
          reject(new Error(translator('Unknow error', lang)));
        }
      });
  });
}

export default removeUser;
