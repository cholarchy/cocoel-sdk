import iGetCommerces from './getCommerces';
import iSuscribeCommercesChanges from './suscribeCommercesChanges';
import iAddCommerce from './addCommerce';
import iGetCommerce from './getCommerce';
import iSuscribeCommerceChanges from './suscribeCommerceChanges';
import iUpdateCommerce from './updateCommerce';
import iStartDeleteCommerce from './startDeleteCommerce';
import iStopDeleteCommerce from './stopDeleteCommerce';

export const getCommerces = iGetCommerces;

export const suscribeCommercesChanges = iSuscribeCommercesChanges;

export const addCommerce = iAddCommerce;

export const getCommerce = iGetCommerce;

export const suscribeCommerceChanges = iSuscribeCommerceChanges;

export const updateCommerce = iUpdateCommerce;

export const startDeleteCommerce = iStartDeleteCommerce;

export const stopDeleteCommerce = iStopDeleteCommerce;
