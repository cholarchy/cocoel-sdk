function suscribeCommercesChanges(cb) {
  if (cb) {
    this.getNewCommerces = () => {
      this.getCommerces()
        .then(newCommercesData => cb(undefined, newCommercesData, this.getCommerces))
        .catch(error => cb(error, undefined, this.getCommerces));
    };
  }

  this.getCommerces().then((newCommercesData) => {
    if (cb) cb(undefined, newCommercesData, this.getCommerces);

    this.socket.on('UPDATED_COMMERCES', this.getNewCommerces);
  }).catch(error => cb(error, undefined, this.getCommerces));
}

export default suscribeCommercesChanges;
