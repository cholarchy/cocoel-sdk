function suscribeUserChanges(cb) {
  if (cb) {
    this.getNewUser = () => {
      this.getUser()
        .then(newUserData => cb(undefined, newUserData, this.getUser))
        .catch(error => cb(error, undefined, this.getUser));
    };
  }

  this.getUser().then((userData) => {
    if (cb) cb(undefined, userData, this.getUser);

    this.socket.on('UPDATED_USER', this.getNewUser);
  }).catch(error => cb(error, undefined, this.getUser));
}

export default suscribeUserChanges;
