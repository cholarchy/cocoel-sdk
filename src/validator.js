import validator from 'validator';

import translator from './translator';

const validators = {
  username(value, lang) {
    if (!value.length) {
      return {
        valid: false,
        message: translator('The Username cannot be empty', lang),
      };
    } else if (value.length < 6) {
      return {
        valid: false,
        message: translator('The Username must have at least 6 characters', lang),
      };
    } else if (value.length > 18) {
      return {
        valid: false,
        message: translator('The Username must not have more than 18 characters', lang),
      };
    } else if (!validator.isAlphanumeric(value)) {
      return {
        valid: false,
        message: translator('The Username must be alphanumeric', lang),
      };
    }

    return {
      valid: true,
      message: null,
    };
  },
  password(value, lang) {
    if (!value.length) {
      return {
        valid: false,
        message: translator('Password cannot be empty', lang),
      };
    } else if (value.length < 6) {
      return {
        valid: false,
        message: translator('Password must have at least 6 characters', lang),
      };
    } else if (value.length > 24) {
      return {
        valid: false,
        message: translator('Password must not have more than 24 characters', lang),
      };
    }

    return {
      valid: true,
      message: null,
    };
  },
  email(value, lang) {
    if (!value.length) {
      return {
        valid: false,
        message: translator('Email cannot be empty', lang),
      };
    } else if (!validator.isEmail(value)) {
      return {
        valid: false,
        message: translator('Email has not valid format', lang),
      };
    }

    return {
      valid: true,
      message: null,
    };
  },
};

export default (value, type, lang = 'es') => validators[type](value, lang);
